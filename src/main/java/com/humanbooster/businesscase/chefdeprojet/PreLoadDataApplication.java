package com.humanbooster.businesscase.chefdeprojet;


import com.humanbooster.businesscase.chefdeprojet.repository.CandidatRepository;
import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SpringBootApplication
public class PreLoadDataApplication {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(PreLoadDataApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PreLoadDataApplication.class);
    }

    @Bean
    public CommandLineRunner PreLoadDataApplication(CandidatRepository repository) {
        return (args) -> {

            List<Candidat> existingCandidats = repository.findAll();

            if (existingCandidats.size() == 0) {
                log.info("-------Crétion d'un candidat !");
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date birthMacron = formatter.parse("21/12/1977");
                repository.save(new Candidat("Macron", "Emmanuel", birthMacron, "Palais de l'Elysée", "Paris", "75000"));
                log.info("-------Création du candidat effectué !");

                log.info("-------Création de plusieurs candidats !");
                List<Candidat> candidats = new ArrayList<Candidat>();
                Date birthAurelien = formatter.parse("30/03/1993");
                Date birthCastex = formatter.parse("25/06/1965");
                Date birthBardet = formatter.parse("09/11/1990");

                candidats.add(new Candidat("Delorme", "Aurélien", birthAurelien, "Stade Marcel Michelin", "Clermont-Ferrand", "63000"));
                candidats.add(new Candidat("Castex", "Jean", birthCastex, "21 Avenue Edmond Berges", "Vic-Fezensac", "32190"));
                candidats.add(new Candidat("Bardet", "Romain", birthCastex, "Avenue thermale", "Royat", "63130"));

                repository.saveAll(candidats);

                log.info("-------Fin du chargement de " + candidats.size() + " candidats !");
            } else {
                log.info("Data already loaded !");
            }

            List<Candidat> candidats = repository.findAll();

            log.info("-------Affichage de tous les candidats !");

            for (Candidat candidat : repository.findAll()) {
                log.info(candidat.toString());
            }

            log.info("-------Affichage des candidats qui ont Aurélien comme prénom !");

            for (Candidat candidat : repository.findByPrenom("Aurélien")) {
                log.info(candidat.toString());
            }

            log.info("-------Affichage des candidats qui ont Macron dans leur noms !");

            for (Candidat candidat : repository.findByNom("Macron")) {
                log.info(candidat.toString());
            }

            log.info("-------Affichage des candidats dans le Puy de dome !");


            for (Candidat candidat : repository.findCandidatByCodePostalContaining("63")) {
                log.info(candidat.toString());
            }

            log.info("-------Affichage paginé de nos resultats !");

            Pageable page = PageRequest.of(0, 2);
            Page<Candidat> candidatsWithPagination = repository.findAllCandidatWithPagination(page);

            while (candidatsWithPagination.getContent().size() != 0) {
                log.info("_ Page " + candidatsWithPagination.getNumber());
                log.info(candidatsWithPagination.getContent().toString());
                page = page.next();
                candidatsWithPagination = repository.findAllCandidatWithPagination(page);
            }

        };
    }
}


/*            */