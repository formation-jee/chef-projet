package com.humanbooster.businesscase.chefdeprojet.controller;


import com.humanbooster.businesscase.chefdeprojet.model.User;
import com.humanbooster.businesscase.chefdeprojet.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/")
public class SecurityController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView mv = new ModelAndView("security/login");

        return  mv;
    }


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView mv = new ModelAndView("security/register");

        mv.addObject("user", new User());

        return  mv;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUserAccount( User user) {

        User existing = userService.findByUsername(user.getUsername());



        userService.save(user);

        return "redirect:/login";
    }
}
