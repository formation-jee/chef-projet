package com.humanbooster.businesscase.chefdeprojet.controller;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import com.humanbooster.businesscase.chefdeprojet.services.CandidatService;
import com.humanbooster.businesscase.chefdeprojet.services.ExcelService;
import com.humanbooster.businesscase.chefdeprojet.services.PdfService;
import com.humanbooster.businesscase.chefdeprojet.services.WordService;
import com.lowagie.text.DocumentException;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(path = "/exports")
public class ExportController {

    @Autowired
    private CandidatService cs;

    @Autowired
    private ExcelService excelService;

    @Autowired
    private WordService wordService;

    @Autowired
    private PdfService pdfService;

    @RequestMapping(value = "/excel", method = RequestMethod.GET)
    public void excel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=export_candidats" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Candidat> listCandidats = cs.getCandidats();


        this.excelService.export(response, listCandidats);

    }

    @RequestMapping(value = "/word", method = RequestMethod.GET)
    public void word(HttpServletResponse response) throws IOException {

        this.wordService.generateWord();

        InputStream inputStream = new FileInputStream(new File("src/main/resources/static/word/rest-with-spring.docx"));
        IOUtils.copy(inputStream, response.getOutputStream());

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=export_candidats" + currentDateTime + ".docx";
        response.setHeader(headerKey, headerValue);

        response.flushBuffer();

    }

    @RequestMapping(value = "/pdf", method = RequestMethod.GET)
    public void pdf(HttpServletResponse response) throws IOException, DocumentException {
        List<Candidat> candidats = this.cs.getCandidats();

        this.pdfService.generatePdfFromHtml(candidats);

        InputStream inputStream = new FileInputStream(new File("src/main/resources/static/pdf/rest-with-spring.pdf"));
        IOUtils.copy(inputStream, response.getOutputStream());

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=export_candidats" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        response.flushBuffer();


    }

}
