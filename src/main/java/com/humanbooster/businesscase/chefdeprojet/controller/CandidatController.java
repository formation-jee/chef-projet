package com.humanbooster.businesscase.chefdeprojet.controller;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import com.humanbooster.businesscase.chefdeprojet.services.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/")
public class CandidatController {

    @Autowired
    private CandidatService candidatService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView candidat() {
        List<Candidat> candidats = this.candidatService.getCandidats();
        ModelAndView mv = new ModelAndView("candidat/list");
        mv.addObject("candidats", candidats);
        return mv;
    }

    @RequestMapping(value = "/{candidat}", method = RequestMethod.GET)
    public ModelAndView candidatDetail(@PathVariable(name="candidat") Candidat candidat) {

        if(candidat == null){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat Not Found");
        }

        ModelAndView mv = new ModelAndView("candidat/detail");
        mv.addObject("candidat", candidat);
        return mv;
    }


    @RequestMapping(value = "test/{candidat}", method = RequestMethod.GET)
    public ModelAndView candidatTest(@PathVariable(name="candidat") Candidat candidat) {

        if(candidat == null){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat Not Found");
        }

        ModelAndView mv = new ModelAndView("candidat/detail");
        mv.addObject("candidat", candidat);
        return mv;
    }

    @RequestMapping(value = "/delete/{candidat}", method = RequestMethod.GET)
    public String deleteCandidat(@PathVariable(name = "candidat") Candidat candidat) {
        this.candidatService.deleteCandidat(candidat);
        return "redirect:/";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addCandidatForm() {
        Candidat candidat = new Candidat();
        ModelAndView mv = new ModelAndView("candidat/form");
        mv.addObject("candidat", candidat);
        return mv;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String saveCandidat(@Valid Candidat candidat, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "candidat/form";
        } else {
            this.candidatService.saveOrUpdateCandidat(candidat);
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/edit/{candidat}", method = RequestMethod.GET)
    public ModelAndView editCandidat(@PathVariable(name = "candidat") Candidat candidat) {

        ModelAndView mv = new ModelAndView("candidat/form");
        mv.addObject("candidat", candidat);

        return mv;
    }

    @RequestMapping(value = "/edit/{candidat}", method = RequestMethod.POST)
    public String editCandidat(@Valid Candidat candidat, BindingResult bindingResult) {

        this.candidatService.saveOrUpdateCandidat(candidat);

        if (bindingResult.hasErrors()) {
            return "candidat/form";
        } else {
            this.candidatService.saveOrUpdateCandidat(candidat);
            return "redirect:/";
        }
    }


}
