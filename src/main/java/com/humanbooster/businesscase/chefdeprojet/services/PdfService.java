package com.humanbooster.businesscase.chefdeprojet.services;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import com.lowagie.text.DocumentException;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Service
public class PdfService {

    public static String output = "src/main/resources/static/pdf/rest-with-spring.pdf";

    private String parseThymeleafTemplate(List<Candidat> candidats) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();

        context.setVariable("candidats", candidats);

        return templateEngine.process("templates/candidat/candidat_pdf_template.html", context);
    }

    public void generatePdfFromHtml(List<Candidat> candidats) throws IOException, DocumentException {
        String html = this.parseThymeleafTemplate(candidats);
        OutputStream outputStream = new FileOutputStream(output);
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);


        outputStream.close();
    }
}
