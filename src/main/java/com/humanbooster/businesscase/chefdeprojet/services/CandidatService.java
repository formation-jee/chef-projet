package com.humanbooster.businesscase.chefdeprojet.services;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import com.humanbooster.businesscase.chefdeprojet.repository.CandidatRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatService {

    private CandidatRepository candidatRepository;

    public CandidatService(CandidatRepository candidatRepository) {
        this.candidatRepository = candidatRepository;
    }

    public List<Candidat> getCandidats() {
        return this.candidatRepository.findAll();
    }

    public Candidat getCandidat(Integer id){
        return  this.candidatRepository.findById(id);
    }

    public void saveOrUpdateCandidat(Candidat candidat) {
        this.candidatRepository.save(candidat);
    }

    public void deleteCandidat(Candidat candidat) {
        this.candidatRepository.delete(candidat);
    }
}
