package com.humanbooster.businesscase.chefdeprojet.services;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class ExcelService {


    private XSSFSheet sheet;
    private List<Candidat> listCandidats;
    private XSSFWorkbook workbook = new XSSFWorkbook();



    private void writeHeaderLine() {

        sheet = workbook.createSheet("Users");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Id");
        createCell(row, 1, "Nom");
        createCell(row, 2, "Prénom");
        createCell(row, 3, "Adresse");
        createCell(row, 4, "Ville");
        createCell(row, 5, "Code postal");
        createCell(row, 6, "Date de naissance");

    }

    private void createCell(Row row, int columnCount, Object value) {
        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle style = workbook.createCellStyle();

        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else if(value instanceof Long){
            cell.setCellValue((Long) value);
        } else if(value instanceof Date) {
            style.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
            cell.setCellStyle(style);
            cell.setCellValue((Date) value);
        } else {
            cell.setCellValue((String) value);
        }
    }

    private void writeDataLines() {

        int rowCount = 1;

        for (Candidat candidat : listCandidats) {

            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;


            createCell(row, columnCount++, candidat.getId());
            createCell(row, columnCount++, candidat.getNom());
            createCell(row, columnCount++, candidat.getPrenom());
            createCell(row, columnCount++, candidat.getAdresse());
            createCell(row, columnCount++, candidat.getVille());
            createCell(row, columnCount++, candidat.getCodePostal());
            createCell(row, columnCount++, candidat.getDateNaissance());


        }
    }

    public void export(HttpServletResponse response, List<Candidat> listCandidats) throws IOException {
        this.listCandidats = listCandidats;
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
