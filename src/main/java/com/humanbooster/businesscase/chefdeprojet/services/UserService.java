package com.humanbooster.businesscase.chefdeprojet.services;



import com.humanbooster.businesscase.chefdeprojet.model.User;
import org.apache.xmlbeans.impl.xb.xsdschema.Attribute;
import org.springframework.security.core.userdetails.UserDetailsService;



public interface UserService extends UserDetailsService {

    User findByUsername(String email);

    User save(User registration);
}