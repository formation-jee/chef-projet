package com.humanbooster.businesscase.chefdeprojet.repository;

import com.humanbooster.businesscase.chefdeprojet.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
