package com.humanbooster.businesscase.chefdeprojet.repository;

import com.humanbooster.businesscase.chefdeprojet.model.Candidat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidatRepository extends CrudRepository<Candidat, Long> {

    List<Candidat> findByPrenom(String prenom);

    List<Candidat> findByNom(String prenom);

    List<Candidat> findAll();

    List<Candidat> findByAdresseAndCodePostalAndVille(String adresse, String codePostal, String ville);

    List<Candidat> findCandidatByCodePostalContaining(String departmentNumber);

    @Query("SELECT c FROM Candidat c WHERE c.adresse = ?1")
    List<Candidat> findCandidatAtAdresseWithHql(String adresse);


    @Query(value = "SELECT u FROM Candidat u ORDER BY id")
    Page<Candidat> findAllCandidatWithPagination(Pageable pageable);


    Candidat findById(long id);
}

