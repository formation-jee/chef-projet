package com.humanbooster.businesscase.chefdeprojet.repository;

import com.humanbooster.businesscase.chefdeprojet.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
